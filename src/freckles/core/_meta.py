# -*- coding: utf-8 -*-
from typing import Any, Dict


project_name = "freckles.core"
project_main_module = "freckles.core"
project_slug = "freckles_core"

pyinstaller: Dict[str, Any] = {}
