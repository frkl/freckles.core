# -*- coding: utf-8 -*-
from typing import Any, Mapping, Optional

from freckles.core.frecklet import Frecklet
from frkl.tasks.task import Task
from frkl.tasks.task_desc import TaskDesc


class FreckletTask(Task):
    def __init__(
        self,
        frecklet: Frecklet,
        input_values: Optional[Mapping[str, Any]] = None,
        task_desc: Optional[TaskDesc] = None,
    ):

        self._frecklet: Frecklet = frecklet
        if input_values is None:
            input_values = {}
        self._input_values: Mapping[str, Any] = input_values

        if task_desc is None:
            task_desc = TaskDesc(
                name=f"executing {self._frecklet.name}", msg=self._frecklet.get_msg()
            )

        self._task_desc = task_desc

        super().__init__(task_desc=self._task_desc)

    async def execute_task(self) -> Any:

        await self._frecklet.add_input_set(**self._input_values)
        frecklet_result = await self._frecklet.frecklecute()

        return frecklet_result.result


# class ParallelFreckletsTask(Tasks):
#
#     def __init__(self, frecklets_map: Mapping[Frecklet, Mapping[str, Any]], task_desc: Optional[TaskDesc]=None, basetopic: Optional[str]=None):
#
#         self._frecklets_map: Mapping[Frecklet, Mapping[str, Any]] = frecklets_map
#
#         if task_desc is None:
#             task_desc = TaskDesc(name="frecklets xxxx")
#
#         self._task_desc: TaskDesc = task_desc
#         if basetopic:
#             self._task_desc.basetopic = basetopic
#         self._task_desc.subtopic = FRECKLES_EVENTS_TOPIC_NAME
#
#         super().__init__(task_desc=task_desc)
#
#     async def initialize_tasklets(self) -> None:
#
#         self._tasklets = []
#         for frecklet, input_values in self._frecklets_map.items():
#
#             await frecklet.add_input_set(**input_values)
#             task: Task = await frecklet.get_frecklet_task()
#             self._tasklets.append(task)
#
#
#     async def execute_tasklets(self, *tasklets: Task) -> None:
#
#         async with create_task_group() as tg:
#             for t in tasklets:
#                 await tg.spawn(t.run_async)
