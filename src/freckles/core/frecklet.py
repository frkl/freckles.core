# -*- coding: utf-8 -*-
import collections
import logging
from abc import abstractmethod
from typing import Any, Dict, List, Mapping, MutableMapping, Optional, Tuple, Union

from frkl.args.arg import Arg, RecordArg
from frkl.args.hive import ArgHive
from frkl.common.exceptions import FrklException
from frkl.common.types import isinstance_or_subclass
from frkl.explain.explanation import Explanation
from frkl.tasks.task import Task, TaskResult
from tings.ting import SimpleTing, TingMeta


log = logging.getLogger("freckles")


def ensure_record_arg_type(
    args: Optional[Union[RecordArg, Mapping[str, Mapping[str, Any]]]], arg_hive: ArgHive
) -> Optional[RecordArg]:

    if not args:
        return None

    if isinstance_or_subclass(args, RecordArg):
        return args  # type: ignore
    elif not isinstance(args, collections.abc.Mapping):
        raise TypeError(
            f"Can't create RecordArg object: invalid args type '{type(args)}'"
        )

    return arg_hive.create_record_arg(childs=args)  # type: ignore


class FreckletVar(object):
    def __init__(self, value: Any, **metadata: Any):

        self._value: Any = value
        self._metadata: MutableMapping[str, Any] = metadata
        self._arg: Optional[Arg] = None

    @property
    def value(self) -> Any:
        return self._value

    @property
    def metadata(self) -> MutableMapping[str, Any]:
        return self._metadata

    @property
    def arg(self) -> Optional[Arg]:
        return self._arg

    @arg.setter
    def arg(self, arg: Arg) -> None:
        self._arg = arg

    def __repr__(self):

        result = f"(FreckletVar: value={self.value}, metadata={self.metadata}"
        if not self.arg:
            result = result + ")"
        else:
            result = " " + result + str(self.arg) + ")"
        return result


class FreckletException(FrklException):
    def __init__(self, *args, frecklet: "Frecklet", **kwargs):

        self._frecklet: Frecklet = frecklet
        super().__init__(*args, **kwargs)

    @property
    def frecklet(self) -> "Frecklet":
        return self._frecklet


class FreckletResult(collections.abc.Mapping, Explanation):
    def __init__(
        self,
        task_result: TaskResult,
        result_schema: Optional[Mapping[str, Mapping[str, Any]]] = None,
    ):

        self._task_result: TaskResult = task_result
        self._result_schema: Optional[Mapping[str, Mapping[str, Any]]] = result_schema
        self._result_value: Optional[Mapping[str, Any]] = None
        Explanation.__init__(self)

    def _check_success(self):

        if not self._task_result.success:
            raise FrklException(
                msg="Can't access result value(s): frecklet run was unsuccessful"
            )

    def _create_result_value(self, task_result: TaskResult) -> Mapping[str, Any]:
        """Create the actual result value dictionary.

        This can be overwritten by specialized result types.
        """

        if isinstance(task_result.result_value, collections.abc.Mapping):
            return task_result.result_value
        else:
            return {"data": task_result.result_value}

    def get_result_schema(self) -> Mapping[str, Mapping[str, Any]]:

        if self._result_schema is None:
            if isinstance(self._task_result.result_value, collections.abc.Mapping):
                _result_schema: Dict[str, Any] = {}
                for k in self._task_result.result_value.keys():
                    _result_schema[k] = "any"
                self._result_schema = _result_schema
            else:
                self._result_schema = {"data": {"type": "dict"}}
        return self._result_schema

    @property
    def result(self) -> Mapping[str, Any]:

        if self._result_value is None:
            self._check_success()
            self._result_value = self._create_result_value(self._task_result)
        return self._result_value

    def get_result_value(self, key: str) -> Any:

        return self.result.get(key, None)

    async def create_explanation_data(self) -> Mapping[str, Any]:

        return self.result

    def __getitem__(self, item):

        return self.result.get(item)

    def __len__(self):

        return self.result.__len__()

    def __iter__(self):

        return self.result.__iter__()


class Frecklet(SimpleTing):
    """Class to hold all information to a generic user-input dependend task.

    The core class in the 'freckles' application."""

    def __init__(
        self, name: str, meta: TingMeta, init_values: Optional[Mapping[str, Any]] = None
    ):

        if init_values is None:
            init_values = {}

        self._init_values: Mapping[str, Any] = init_values
        self._current_frecklet_input: Dict[str, Mapping[str, Any]] = {}

        self._processed_input_values: Dict[str, Any] = {}

        super().__init__(name=name, meta=meta)

        self._msg = f"processing frecklet '{self.name}'"

        self._vals_stash: Dict[str, FreckletVar] = {}
        self._current_required_args: Optional[RecordArg] = ensure_record_arg_type(
            self.get_required_base_args(), arg_hive=self.tingistry.arg_hive
        )
        self._previous_input_sets: List[Mapping[str, Any]] = []
        self._user_input_set: bool = False

        self._base_namespace = ".".join(self.full_name.split(".")[0:-2])

    def invalidate_frecklet(self):

        self._current_frecklet_input.clear()
        self._current_required_args = ensure_record_arg_type(
            self.get_required_base_args(), arg_hive=self.tingistry.arg_hive
        )
        self._previous_input_sets.clear()
        self._processed_input_values.clear()
        self._user_input_set: bool = False
        self.invalidate()

    @property
    def base_namespace(self) -> str:
        return self._base_namespace

    @property
    def current_frecklet_input_details(self) -> Mapping[str, Mapping[str, Any]]:

        return self._current_frecklet_input

    def set_processed_input(self, key: str, value: Any) -> None:

        self._processed_input_values[key] = value

    def get_processed_input(self, key: str):

        return self._processed_input_values[key]

    @property
    def current_frecklet_values(self) -> Mapping[str, Any]:
        return {k: v["validated"] for k, v in self._current_frecklet_input.items()}

    # def get_input_details(self, key: str) -> FreckletVar:
    #
    #     return self._current_frecklet_input.get(key)

    def get_msg(self) -> str:

        return self._msg

    async def get_frecklet_task(self) -> Task:

        while self.get_current_required_args():
            await self.add_input_set()

        task: Task = await self.get_value("task", raise_exception=True)
        return task

    async def frecklecute(self) -> FreckletResult:

        task: Task = await self.get_frecklet_task()

        if not task.is_finished:

            try:

                await task.run_async()

            finally:
                pass
                # twm.remove_watcher(wid)

        if not task.result.success:
            exc: Exception = task.result.error  # type: ignore
            raise exc

        if not isinstance_or_subclass(task.result, FreckletResult):
            result_value: FreckletResult = self.process_frecklet_result(task.result)
            if not isinstance_or_subclass(result_value, FreckletResult):
                raise TypeError(
                    f"Invalid result type from '{self.__class__.__name__}' (must be 'FreckletResult': {type(result_value)}"
                )
        else:
            result_value = task.result  # type: ignore
        return result_value

    def process_frecklet_result(self, result: TaskResult) -> FreckletResult:
        return FreckletResult(result)

    def requires(self) -> Mapping[str, Union[str, Mapping[str, Any]]]:

        return {"input_details": "dict"}

    def provides(self):

        return {"task": "any?", "input_values": "dict", "input_details": "dict"}

    async def retrieve(self, *value_names: str, **requirements) -> Mapping[str, Any]:

        result = {}
        if "input_details" in value_names:
            result["input_details"] = requirements["input_details"]

        input_vals = None
        if "input_values" in value_names:
            input_vals = {}
            for k, v in requirements["input_details"].items():
                input_vals[k] = v["validated"]
            result["input_values"] = input_vals

        if "task" in value_names:
            if input_vals is None:
                input_vals = {}
                for k, v in requirements["input_details"].items():
                    input_vals[k] = v["validated"]

            task = await self._create_frecklet_task(**input_vals)
            task.task_desc.topic = self.base_namespace
            result["task"] = task

        return result

    def is_saturated(self) -> bool:

        return not self.get_current_required_args()

    @property
    def current_amount_of_inputs(self) -> int:
        """Return how many input sets were commited so far."""

        return len(self._previous_input_sets)

    @abstractmethod
    def get_required_base_args(
        self,
    ) -> Optional[Union[RecordArg, Mapping[str, Mapping[str, Any]]]]:
        pass

    async def get_default_input(self) -> Mapping[str, Union[FreckletVar, Any]]:
        return {}

    def get_current_required_args(self) -> Optional[RecordArg]:
        return self._current_required_args

    async def add_input_set(
        self,
        _default_metadata: Optional[Mapping[str, Any]] = None,
        **input_vars: Union[FreckletVar, Any],
    ) -> Optional[RecordArg]:
        """Add a set of input values for the currently required (or anticipated future) arguments."""

        # # before we do anything else, we make sure defaults are ready
        # if not self._user_input_set:
        #     default_input = await self.get_default_input()
        #     for k, v in default_input.items():
        #         if not isinstance_or_subclass(v, FreckletVar):
        #             _v = FreckletVar(v)
        #         else:
        #             _v = v
        #         if not _v.metadata.get("origin", None):
        #             _v.metadata["origin"] = "frecklet defaults"
        #         self._vals_stash[k]= _v

        self._user_input_set = True

        if _default_metadata is None:
            _default_metadata = {}

        if "value" in _default_metadata:
            raise Exception("Default metadata can't contain key 'value'.")

        if not self.get_current_required_args():
            raise FreckletException(
                frecklet=self,
                msg=f"Invalid input to frecklet '{self.name}'.",
                reason="No input required.",
            )

        # Make sure all input vars are 'FreckletVar's
        input_frecklet_vars = {}
        for k, v in input_vars.items():
            if isinstance_or_subclass(v, FreckletVar):
                input_frecklet_vars[k] = v
            else:
                _v = FreckletVar(v, **_default_metadata)
                input_frecklet_vars[k] = _v

        # gather all variables that are currently required
        relevant_vars: Dict[str, FreckletVar] = {}
        for key, arg in self.get_current_required_args().childs.items():  # type: ignore

            if key not in input_frecklet_vars.keys():
                if key in self._vals_stash.keys():
                    # var as provided in an earlier run
                    relevant_vars[key] = self._vals_stash.pop(key)
                else:
                    # missing, might be resolved by a default when validating
                    continue
            else:
                # we can use the var directly
                relevant_vars[key] = input_frecklet_vars.pop(key)

        # now we gather all vars that are not currently required, but might be further down the line
        not_relevant_vars: Dict[str, FreckletVar] = {}
        for k, v in input_frecklet_vars.items():

            if isinstance_or_subclass(v, FreckletVar):
                not_relevant_vars[k] = v
            else:
                _v = FreckletVar(v, **_default_metadata)
                not_relevant_vars[k] = _v

        # now we extract the actual input values, and also check whether there is duplicate input for a key
        # which would indicate some sort of mishap
        valid_input: Dict[str, Any] = {}
        duplicate: List[str] = []

        # check that we don't have duplicate input
        for key, value in relevant_vars.items():

            if key in self._vals_stash.keys():
                self._vals_stash.pop(key)

            if key in self._current_frecklet_input.keys():
                duplicate.append(key)
                continue

            valid_input[key] = value.value

        if duplicate:
            self.invalidate_frecklet()
            raise FreckletException(
                frecklet=self,
                msg=f"Invalid input to frecklet '{self.name}'.",
                reason=f"Already values provided for: '{', '.join(duplicate)}",
            )

        # now we inform the inheriting frecklet that we got new input values, those are not validated yet
        # the frecklet looks at those, and tells us whether it needs more input
        # it can also return values and defaults for some of the current (or future) values

        _new_args = await self.input_received(**relevant_vars)
        if not _new_args:
            defaults: Mapping[str, Union[FreckletVar, Any]] = {}
            new_args: Optional[RecordArg] = None
        elif isinstance_or_subclass(_new_args, RecordArg):
            new_args = _new_args  # type: ignore
            defaults = {}
        elif isinstance(_new_args, collections.abc.Mapping):
            new_args = ensure_record_arg_type(
                _new_args, arg_hive=self.tingistry.arg_hive
            )
            defaults = {}
        elif isinstance(_new_args, tuple):
            if len(_new_args) == 1:
                new_args = _new_args[0]
                defaults = {}
            elif len(_new_args) == 2:
                new_args = _new_args[0]
                defaults = _new_args[1]
            else:
                raise Exception("Invalid result tuple length.")
        else:
            raise TypeError(
                f"Invalid type '{type(_new_args)}' for 'input_received' method. This is a bug."
            )

        # now we process the returned default values
        # first we check whether the key is among the currently required ones, if so, and not provided by user manually,
        # add it to the input. otherwise, add it to the stash.
        for k, v in defaults.items():
            if isinstance_or_subclass(v, FreckletVar):
                _v = v
            else:
                _v = FreckletVar(v)

            if not _v.metadata.get("origin", None):
                _v.metadata["origin"] = "processed value"

            if k in self._current_required_args.arg_names:  # type: ignore
                if k in input_vars.keys():
                    log.debug(f"Overwriting input for {k} with processed value.")

                relevant_vars[k] = _v
                valid_input[k] = _v.value
            else:
                self._vals_stash[k] = _v

        # update the stash with potential user input for future input adds
        self._vals_stash.update(not_relevant_vars)

        # validate the input for this round
        validated = self._current_required_args.validate(  # type: ignore
            valid_input, raise_exception=True
        )
        # validation was successful, we now need to know whether there will be another round

        # now we 'commit' the current round input
        for k, arg in self._current_required_args.childs.items():  # type: ignore
            v = validated[k]
            frecklet_var = relevant_vars.get(k, None)
            if frecklet_var is None:
                origin = None if v is None else "frecklet default"
                if origin:
                    frecklet_var = FreckletVar(None, origin=origin)
                else:
                    frecklet_var = FreckletVar(None)
            self._current_frecklet_input[k] = {
                "validated": v,
                "var": frecklet_var,
                "arg": arg,
            }

        # set the new required args (if any)
        self._current_required_args = ensure_record_arg_type(
            new_args, arg_hive=self.tingistry.arg_hive
        )
        # add relevant input vars to this frecklets input history
        self._previous_input_sets.append(relevant_vars)

        # if no other input is required, commit the input to the frecklet proper
        if not self._current_required_args:
            self._set_final_input_values()

        return self._current_required_args

    def _set_final_input_values(self):

        if self._current_required_args:
            raise FreckletException(
                msg=f"Can't set input values for frecklet '{self.name}'.",
                reason=f"Some arguments not set yet: {self._current_required_args}.",
            )

        values: Dict[str, Any] = {}
        for k, details in self._current_frecklet_input.items():
            values[k] = details

        self.set_input(input_details=values)

    @abstractmethod
    async def input_received(
        self, **input_vars: FreckletVar
    ) -> Optional[Union[RecordArg, Tuple]]:
        pass

    @abstractmethod
    async def _create_frecklet_task(self, **input_values: Any) -> Task:
        pass


# class SimpleArgsFrecklet(Frecklet):
#
#     _ting_type_name = "simple_args"
#
#     def __init__(
#         self, name: str, meta: TingMeta, init_values: Optional[Mapping[str, Any]] = None
#     ):
#
#         self._args: Optional[RecordArg] = None
#
#         super().__init__(name=name, meta=meta, init_values=init_values)
#
#     @property
#     def args(self) -> Optional[RecordArg]:
#         return self._args
#
#     @args.setter
#     def args(self, args: RecordArg) -> None:
#         self._args = args
#         self.invalidate_frecklet()
#
#     async def input_received(self, **input_vars: FreckletVar) -> Optional[RecordArg]:
#         return None
#
#     def get_required_base_args(self) -> Optional[RecordArg]:
#
#         return self._args
#
#     async def _create_frecklet_task(self, **input_values: Any) -> Task:
#
#         return input_values
