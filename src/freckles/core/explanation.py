# -*- coding: utf-8 -*-
import collections
from typing import Any, Dict, Mapping

from frkl.common.formats.serialize import to_value_string
from frkl.explain.explanation import DataExplanation
from rich import box
from rich.console import Console, ConsoleOptions, RenderResult
from rich.table import Table


class FreckletInputExplanation(DataExplanation):

    _plugin_name = "frecklet_input"
    _plugin_supports = ["frecklet_input"]

    def __init__(self, data: Any):

        super().__init__(data=data, data_cls=collections.abc.Mapping)

    async def create_explanation_data(self) -> Mapping[str, Any]:

        frecklet_input: Mapping[str, Mapping[str, Any]] = self.data

        values: Dict[str, Any] = {}
        for k, v in sorted(frecklet_input.items()):
            raw_value = v["var"].value
            origin = v["var"].metadata.get("origin", None)
            values.setdefault(k, {})["value"] = raw_value
            if origin:
                values[k]["origin"] = origin
            if "from_alias" in v["var"].metadata.keys():
                values[k]["from_alias"] = v["var"].metadata["from_alias"]
        return values

    def __rich_console__(
        self, console: Console, options: ConsoleOptions
    ) -> RenderResult:

        yield "[title]Variables[/title]"

        value_dict: Mapping[str, Any] = self.explanation_data

        table = Table(show_header=False, box=box.SIMPLE)
        table.add_column("Name", no_wrap=True, style="key2")
        table.add_column("Value", style="value")

        # use_alias = False
        # for data in value_dict.values():
        #
        #     if "from_alias" in data.keys():
        #         use_alias = True
        #         break
        #
        # if use_alias:
        #     table.add_column("from Alias", style="value")

        table.add_column("Origin", style="value")

        for arg_name, data in value_dict.items():
            value = data["value"]
            origin = data.get("origin", "")

            value_string = to_value_string(value)
            metadata_strings = [] if not origin else [f"origin: {origin}"]

            from_alias = data.get("from_alias", None)
            if from_alias:
                metadata_strings.append(f"from alias: {from_alias}")

            # if not use_alias:
            table.add_row(arg_name, value_string, ", ".join(metadata_strings))
            # else:
            #     alias = data.get("from_alias", "")
            #     table.add_row(arg_name, value_string, alias, f"origin: {origin}")

        yield table


# class FreckletResultExplanation(Explanation):
#
#     async def create_explanation_data(self) -> Mapping[str, Any]:


# class FreckletExplanation(Explanation):
#     def __init__(self, frecklet: "Frecklet", **kwargs):
#
#         super().__init__(data=frecklet, **kwargs)
#
#     async def create_explanation_data(self) -> Mapping[str, Any]:
#
#         frecklet: Frecklet = self.data
#
#         task: Task = await frecklet.get_value("task")  # type: ignore
#         if task is None:
#             raise NotImplementedError()
#         task_expl = TaskExplanation(task)
#
#         frecklet_input = frecklet.input_sets.explain()
#         frecklet_input_dict = frecklet_input.explanation_data
#
#         args = await frecklet._get_required_args()
#
#         vars = await frecklet.get_vars()
#
#         vars_dict = {}
#         for k, v in sorted(vars.items()):
#
#             metadata = frecklet_input_dict.get(k, None)
#
#             if metadata is None:
#                 origin = "-- n/a --"
#             else:
#                 origin = metadata["origin"]
#
#             is_set = v is not None
#
#             arg = args.get_child_arg(k)
#             if arg is None:
#                 raise NotImplementedError()
#             vars_dict[k] = {
#                 "value": v,
#                 "desc": arg.doc.get_short_help("-- no description --"),
#                 "origin": origin,
#                 "is_set": is_set,
#             }
#             if metadata and "from_alias" in metadata.keys():
#                 vars_dict[k]["from_alias"] = metadata["from_alias"]
#
#         result = {
#             "task": task_expl,
#             "input": frecklet_input,
#             "vars": vars_dict,
#             "args": args,
#         }
#         return result
#
#     def render_vars_table(self, vars_dict: Mapping[str, Mapping[str, Any]]) -> Table:
#
#         aliases: bool = False
#         for arg_name, data in vars_dict.items():
#             _alias = data.get("from_alias", None)
#             if _alias:
#                 aliases = True
#                 break
#
#         table = Table(show_header=True, box=box.SIMPLE)
#         table.add_column("Name", no_wrap=True, style="key2")
#         table.add_column("Value", style="value")
#
#         if aliases:
#             table.add_column("from alias", no_wrap=True, style="value")
#
#         table.add_column("Origin")
#
#         for arg_name, data in vars_dict.items():
#             if aliases:
#                 _alias = data.get("from_alias", "")
#             is_set = data["is_set"]
#             if is_set:
#                 value = data["value"]
#                 origin = data["origin"]
#             else:
#                 value = "-- not set --"
#                 origin = ""
#
#             value_string = to_value_string(value)
#
#             if aliases:
#                 table.add_row(arg_name, value_string, _alias, origin)
#             else:
#                 table.add_row(arg_name, value_string, origin)
#
#         return table
#
#     def _create_task_tree_item(
#         self, task_data: Mapping[str, Any], level: int = 0, current: List[str] = None
#     ):
#
#         if current is None:
#             current = []
#
#         padding = ("  " * level) + "- "
#
#         _meta = task_data["meta"]
#         _postprocess = task_data.get("postprocess", None)
#         _subtasks = task_data.get("subtasks", [])
#
#         msg = _meta["msg"]
#
#         if level == 0:
#             current.append(f"{padding}{msg}")
#         else:
#             color_number = (level - 1) % 5
#             color = FRKL_COLOR_PROGRESSION[color_number]
#             current.append(f"[{color}]{padding}{msg}[/{color}]")
#
#         for task in _subtasks:
#             self._create_task_tree_item(task, level=level + 1, current=current)
#
#         if _postprocess:
#             self._create_task_tree_item(_postprocess, level=level + 1, current=current)
#
#         return current
#
#     def __rich_console__(
#         self, console: Console, options: ConsoleOptions
#     ) -> RenderResult:
#
#         _vars: Mapping[str, Any] = self.explain("vars")
#         # _tasks = self.explain("task.subtasks")
#
#         msg = self.explain("task.meta.msg")
#         yield f"[title]Task:[/title] {msg}"
#         yield ""
#         yield "[title]Variables:[/title]"
#         vars = self.render_vars_table(_vars)
#         yield vars
#
#         yield ""
#         yield "[title]Steps:[/title]"
#         yield ""
#
#         items = self._create_task_tree_item(self.explanation_data["task"])
#         for item in items:
#             yield item
