# -*- coding: utf-8 -*-
import os
import sys
from typing import Any, Mapping

from appdirs import AppDirs


freckles_core_app_dirs = AppDirs("freckles_core", "frkl")

if not hasattr(sys, "frozen"):
    FRECKLES_CORE_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `freckles_core` module."""
else:
    FRECKLES_CORE_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "freckles_core"  # type: ignore
    )
    """Marker to indicate the base folder for the `freckles_core` module."""

FRECKLES_CORE_RESOURCES_FOLDER = os.path.join(
    FRECKLES_CORE_MODULE_BASE_FOLDER, "resources"
)


FRECKLES_INIT: Mapping[str, Any] = {"prototings": []}

FRECKLES_EVENTS_TOPIC_NAME = "events"
