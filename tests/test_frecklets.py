#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `freckles_core` package."""
from typing import Any, Optional, Tuple, Union

import pytest  # noqa
from freckles.core.frecklet import Frecklet
from frkl.args.arg import RecordArg
from frkl.args.exceptions import ArgValidationError
from frkl.tasks.task import Task


# @pytest.mark.anyio
# async def test_create_frecklet(tingistry):
#
#     args = {"x": "string"}
#     record_arg = tingistry.arg_hive.create_record_arg(args)
#
#     frecklet = tingistry.create_ting("simple_args")
#     frecklet.args = record_arg
#
#     assert isinstance_or_subclass(frecklet, Frecklet)
#
#     with pytest.raises(TingTaskException):
#         await frecklet.get_values("input_details", raise_exception=True)


# @pytest.mark.anyio
# async def test_dict_frecklet(tingistry):
#
#     args = {"x": "string", "z": "string?", "a": {"default": "aaaa", "type": "string"}}
#     record_arg = tingistry.arg_hive.create_record_arg(args)
#
#     frecklet: Frecklet = tingistry.create_ting("simple_args")
#     frecklet.args = record_arg
#
#     mv = frecklet.get_current_required_args()
#     assert "x" in mv.childs.keys()
#
#     assert not frecklet.is_saturated()
#
#     x = await frecklet.add_input_set(x="1", y=2)
#     assert x is None
#     assert frecklet.is_saturated()
#
#     vals = await frecklet.get_values()
#     assert vals["input_values"] == {"a": "aaaa", "x": "1", "z": None}
#
#     input_details = vals["input_details"]
#     assert "a" in input_details.keys()
#     assert "x" in input_details.keys()
#     assert "z" in input_details.keys()
#
#     assert input_details["a"]["var"].value is None
#     assert input_details["x"]["var"].value == "1"
#     assert input_details["z"]["var"].value is None
#
#     assert vals["input_values"] == vals["task"]


@pytest.mark.anyio
async def test_args_frecklet(tingistry):
    class TestFrecklet(Frecklet):

        _plugin_name = "test1"

        def get_required_base_args(self) -> Optional[RecordArg]:

            return {"test_arg_1": "string"}

        async def input_received(
            self, **input_vars: Any
        ) -> Optional[Union[RecordArg, Tuple]]:
            pass

        def _create_frecklet_task(self, **input_values: Any) -> Task:
            pass

    frecklet: Frecklet = tingistry.create_ting("test1", "test.ting")
    args = frecklet.get_current_required_args()
    assert "test_arg_1" in args.childs.keys()
    await frecklet.add_input_set(test_arg_1="xxx")
    args = frecklet.get_current_required_args()
    assert not args


@pytest.mark.anyio
async def test_args_frecklet_multiple_input_sets(tingistry):
    class TestFrecklet2(Frecklet):

        _plugin_name = "test2"

        def get_required_base_args(self) -> Optional[RecordArg]:

            return {"test_arg_1": "string", "test_arg_2": "string"}

        async def input_received(
            self, **input_vars: Any
        ) -> Optional[Union[RecordArg, Tuple]]:

            if self.current_amount_of_inputs == 1:
                return None

            return {"test_arg_3": "string"}

        def _create_frecklet_task(self, **input_values: Any) -> Task:
            pass

    frecklet: Frecklet = tingistry.create_ting("test2", "test.ting")
    args = frecklet.get_current_required_args()
    assert "test_arg_1" in args.childs.keys()
    assert "test_arg_2" in args.childs.keys()
    assert "test_arg_3" not in args.childs.keys()

    new_args = await frecklet.add_input_set(test_arg_1="x", test_arg_2="y")
    args_2 = frecklet.get_current_required_args()

    assert new_args == args_2
    assert "test_arg_3" in args_2.childs.keys()

    with pytest.raises(ArgValidationError):
        await frecklet.add_input_set()
    new_args_2 = await frecklet.add_input_set(test_arg_3="z")
    assert not new_args_2


@pytest.mark.anyio
async def test_args_frecklet_multiple_input_sets_2(tingistry):
    class TestFrecklet3(Frecklet):

        _plugin_name = "test3"

        def get_required_base_args(self) -> Optional[RecordArg]:

            return {"test_arg_1": "string", "test_arg_2": "string"}

        async def input_received(
            self, **input_vars: Any
        ) -> Optional[Union[RecordArg, Tuple]]:

            if self.current_amount_of_inputs == 1:
                return None

            return {"test_arg_3": "string"}

        def _create_frecklet_task(self, **input_values: Any) -> Task:
            pass

    frecklet: Frecklet = tingistry.create_ting("test3", "test.ting_2")
    args = await frecklet.add_input_set(test_arg_1="x", test_arg_2="y", test_arg_3="z")
    args = frecklet.get_current_required_args()
    assert "test_arg_3" in args.childs.keys()
    assert "test_arg_3" in frecklet._vals_stash.keys()
    args = await frecklet.add_input_set()
    assert not args
    assert "test_arg_3" not in frecklet._vals_stash.keys()
