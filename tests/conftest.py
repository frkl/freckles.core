#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Dummy conftest.py for freckles.core.

    If you don't know what this is for, just leave it empty.
    Read more about conftest.py under:
    https://pytest.org/latest/plugins.html
"""
# import pytest
import random
import string

import pytest
from tings.tingistry import Tingistries, Tingistry


def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(stringLength))


@pytest.fixture
def tingistry() -> Tingistry:

    Tingistries().remove_all_tingistries()

    return Tingistries().add_tingistry(
        randomString(8), modules=["freckles.core.frecklet"]
    )
